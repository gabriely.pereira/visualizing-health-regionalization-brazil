'''
    Creates the communities shape files for a state for available year data.
    
    Columns: ['year', 'geometry'] for regions and macroregions

    Run: python3 create_communities_shapes.py <UF> <regional|macroregional> <diagnosis(optional)>

    Creates "reg_communities.shp" and "macro_communities.shp", and also
            "reg_similarity.csv" and "macro_similarity.csv" for a specific Brazilian state.
'''

import sys
import infomap
import pandas as pd
import geopandas as gpd
import numpy as np
import networkx as nx

from os.path import exists
from sklearn.metrics.cluster import adjusted_rand_score


if(len(sys.argv) < 3): sys.exit("[error] run: python3 create_communities_shapes.py <UF> <regional|macroregional> <diagnosis(optional)>")
if(sys.argv[2] != "regional" and sys.argv[2] != "macroregional"): sys.exit("[error] Invalid value. Valid options: regional macroregional")

# get state abbreviation (UF)
UF = sys.argv[1]

# get diagnosis
if(len(sys.argv) > 3): diagnosis = sys.argv[3]+'_'
else: diagnosis = ''

# import the municipalities shapes from the IBGE municipalities dataset (2021)
#   -> (https://www.ibge.gov.br/geociencias/organizacao-do-territorio/malhas-territoriais)
mun_df = gpd.read_file("../../data/%s/%s_Municipios_2021.dbf"%(UF, UF), encoding = "utf-8")
mun_df['code'] = np.array([item[:-1] for item in np.array(mun_df.CD_MUN)]).astype('int') # trim last char

# import the municipality-region-macroregion relations
reg_macro_df = pd.read_csv("../../data/%s/reg_macro_table.csv"%UF)

# import the flow data for each available year
if(diagnosis == ''): df = pd.read_csv("../../data/%s/template_dashboard/csv/mun_sih_flow.csv"%UF)
else: df = pd.read_csv("../../data/%s/template_dashboard/csv/diagnosis/mun/mun_%ssih_flow.csv"%(UF, diagnosis))

# create/open file to save the similarity index
if(sys.argv[2]=='regional'): r = 'reg'
else: r = 'macro'
if(exists("../../data/%s/template_dashboard/csv/similarity/%s.csv"%(UF, r))):
    index_df = pd.read_csv("../../data/%s/template_dashboard/csv/similarity/%s.csv"%(UF, r), index_col='year')
else:
    index_df = pd.DataFrame({'year': df.sort_values(by='year')['year'].unique(),
                            'all': ['']*len(df['year'].unique())}).set_index('year')
if(len(sys.argv) > 3): index_df[diagnosis[:-1]] = ['']*len(index_df)

# create final shape dataframe
communities_gdf = gpd.GeoDataFrame({'year': [''],
                                    'geometry': ['']}, index=[0]) 

# Partition network with the Infomap algorithm
def find_communities(G, regions_num, v=False):

    # limit number of modules to X (same number of health reagions)
    im = infomap.Infomap("--silent --preferred-number-of-modules %d"%regions_num)

    if v: print("Building Infomap network from a NetworkX graph...")
    for source, target, D in G.edges(data=True):
        s, t = eval(source)['code'], eval(target)['code']
        if D:
            im.add_link(s, t, weight=D['hospitalizations'])
        else:
            im.add_link(s, t)    
            
    for node in G.nodes:
        im.add_node(eval(node)['code'])

    if v: print("Find communities with Infomap...")
    im.run()

    if v: print(f"Found {im.num_top_modules} modules with codelength: {im.codelength}")

    communities = im.get_modules()
    nx.set_node_attributes(G, communities, 'community')
    
    return im

for year in df['year'].unique():
    available_year = year
    if year < 2011: 
        available_year = 2011
        if UF == 'SP': available_year = 2012
    if year == 2018: available_year = 2017

    # get available municipality-region-macroregion relations and the number of health regions/macroregions
    if(sys.argv[2]=='regional'): 
        year_reg_macro_df = reg_macro_df[reg_macro_df['year']==available_year]
        regions_num = len(year_reg_macro_df['region'].unique())
    else: 
        year_reg_macro_df = reg_macro_df[reg_macro_df['year']==2019]
        regions_num = len(year_reg_macro_df['macroregion'].unique())

    year_df = df[df['year']==year]

    year_df = year_df[['from', 'to', 'hospitalizations']].groupby(
        by = ['from', 'to'], as_index = False).sum()

    graph = nx.from_pandas_edgelist(year_df, source = 'from', target = 'to',
                    edge_attr = 'hospitalizations',create_using = nx.DiGraph())

    im = find_communities(graph, regions_num)

    reg = 'region'
    if(sys.argv[2]=='macroregional'): reg = 'macroregion'
    R = {row['code']:row[reg] for _, row in year_reg_macro_df.sort_values(by='code').iterrows()}

    year_df['from_code'] = year_df['from'].map(lambda x: eval(x)['code'])
    for code in year_reg_macro_df[year_reg_macro_df['code'].isin(year_df['from_code'].unique())==False]['code']:
        R.pop(code, None)

    index = ''
    if(len(list(R.values()))>1 and len(list(R.values())) == len(list(im.get_modules().values()))): index = adjusted_rand_score(list(R.values()), list(im.get_modules().values()))*100
    index_name = 'Adjusted Rand Index'

    print(year, index)
    if(len(sys.argv) > 3): index_df.loc[int(year), diagnosis[:-1]] = index
    else: index_df.loc[int(year), 'all'] = index

    infomap_result = pd.DataFrame(im.modules, columns=['mun_code', 'community'])
    mun_df = mun_df.merge(infomap_result, how = 'left', left_on = ['code'], right_on = ['mun_code'])
    dissolved = mun_df[['community', 'geometry']].dissolve(by='community')
    dissolved['year'] = int(year)

    communities_gdf = pd.concat([communities_gdf, dissolved[['year', 'geometry']]], axis=0, sort=False, ignore_index=True)
    mun_df = mun_df.drop(columns=['mun_code', 'community'])

# saving communities shapes and similarity index to file
communities_gdf = communities_gdf[1:]
communities_gdf['geometry'] = communities_gdf['geometry'].simplify(tolerance=0.01)
if(sys.argv[2]=='regional'): 
    communities_gdf.to_file("../../data/%s/template_dashboard/shapes/communities/reg/%scommunities.shp"%(UF, diagnosis))
    index_df.to_csv("../../data/%s/template_dashboard/csv/similarity/reg.csv"%UF)
else: 
    communities_gdf.to_file("../../data/%s/template_dashboard/shapes/communities/macro/%scommunities.shp"%(UF, diagnosis))
    index_df.to_csv("../../data/%s/template_dashboard/csv/similarity/macro.csv"%UF)

print(communities_gdf)
