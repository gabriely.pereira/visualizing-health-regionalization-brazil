'''
    Creates the SHAPE files for a state for available year data.
    
    Columns: ['name', 'year', 'geometry'] for regions and macroregions
             ['name', 'geometry'] for state

    Run: python3 create_shapes.py <UF>

    Creates "state.shp", "regions.shp" and "macroregions.shp" for a specific Brazilian state.
'''

import sys
import pandas as pd
import geopandas as gpd
import numpy as np

from shapely.ops import cascaded_union
from shapely.geometry import Polygon

# get state abbreviation (UF)
UF = sys.argv[1]

# import the municipalities shapes from the IBGE municipalities dataset (2021)
#   -> (https://www.ibge.gov.br/geociencias/organizacao-do-territorio/malhas-territoriais)
mun_df = gpd.read_file("../../data/%s/%s_Municipios_2021.dbf"%(UF, UF), encoding = "utf-8")
mun_df['code'] = np.array([item[:-1] for item in np.array(mun_df.CD_MUN)]).astype('int') # trim last char

# import the municipality-region-macroregion relations
reg_macro_df = pd.read_csv("../../data/%s/reg_macro_table.csv"%UF)

# ---------MACROREGION----------
regions_boundary, names, years = [], [], []
for year in [2019]:
    mun_df['macroregion'] = reg_macro_df[reg_macro_df['year']==year].macroregion.values
    dissolved = mun_df[['macroregion', 'geometry']].dissolve(by='macroregion')
    regions_boundary += dissolved.geometry.to_list()
    years += [year]*len(dissolved)
    names += dissolved.index.to_list()

# saving regions shape to file
macroregions_gdf = gpd.GeoDataFrame({
    'name': names,
    'year': years,
    'geometry': regions_boundary
})
macroregions_gdf['geometry'] = macroregions_gdf['geometry'].simplify(tolerance=0.01)
macroregions_gdf['center'] = macroregions_gdf['geometry'].centroid
macroregions_gdf['center'] = macroregions_gdf['center'].apply(lambda point: str([point.x, point.y]))
macroregions_gdf.to_file("../../data/%s/template_dashboard/shapes/macroregions/shape.shp"%UF)
print(macroregions_gdf)

# ------------REGION------------
regions_boundary, names, years = [], [], []
init = 2011
if(UF == 'SP'): init = 2012
for year in list(range(init, 2017+1))+[2019]:
    mun_df['region'] = reg_macro_df[reg_macro_df['year']==year].set_index('region').index.values
    dissolved = mun_df[['region', 'geometry']].dissolve(by='region')
    regions_boundary += dissolved.geometry.to_list()
    years += [year]*len(dissolved)
    names += dissolved.index.to_list()

# saving regions shape to file
regions_gdf = gpd.GeoDataFrame({
    'name': names,
    'year': years,
    'geometry': regions_boundary
})

regions_gdf['geometry'] = regions_gdf['geometry'].simplify(tolerance=0.01)
regions_gdf['center'] = regions_gdf['geometry'].centroid
regions_gdf['center'] = regions_gdf['center'].apply(lambda point: str([point.x, point.y]))
regions_gdf.to_file("../../data/%s/template_dashboard/shapes/regions/shape.shp"%UF)
print(regions_gdf)

# ------------STATE-------------
boundary = cascaded_union(regions_boundary)

state_gdf = gpd.GeoDataFrame({
    'name': UF,
    'geometry': [boundary],
})

state_gdf['geometry'] = state_gdf['geometry'].simplify(tolerance=0.01)
state_gdf['center'] = state_gdf['geometry'].centroid
state_gdf['center'] = state_gdf['center'].apply(lambda point: str([point.x, point.y]))
state_gdf.to_file("../../data/%s/template_dashboard/shapes/state/shape.shp"%(UF))
print(state_gdf)
