import pandas as pd

region = 'COSU' #'NOBA'
fname = "../../../../Downloads/SIH/SIH_%s.dta"%region

def load_large_dta(fname):
    import sys

    reader = pd.read_stata(fname, columns=['ano_adm', 'mun_res', 'mun_cnes', 'cid_pri10', 'cid_pri9', 'complex'], iterator=True)
    n = 0

    try:
        chunk = reader.get_chunk(100*1000)
        while len(chunk) > 0:
            chunk.to_csv("../../../%s/parts/%d_CID.csv"%(region, n), index=False)
            chunk = reader.get_chunk(100*1000)
            sys.stdout.flush()
            n+=1
    except (StopIteration, KeyboardInterrupt):
        pass

    return(n)
    
maxx = load_large_dta(fname)
print(">>", maxx)


i, n = 0, maxx+1
df = pd.read_csv("../../../%s/parts/%d_CID.csv"%(region, i)).dropna(subset=['mun_res', 'mun_cnes'])

for i in range(1, n):
    df = df.append(pd.read_csv("../../../%s/parts/%d_CID.csv"%(region, i)).dropna(subset=['mun_res', 'mun_cnes']))
    print(i)

print(df)

df.to_csv("../../../%s/%s_CID.csv"%(region, region), index=False)
