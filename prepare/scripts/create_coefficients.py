'''
    Creates the files with (performance) coefficients for a state for available year data.
    
    Columns: ['year', 'name', 'permanence', 'attraction'] for regions and macroregions

    Run: python3 create_coefficients.py <UF> <regional|macroregional>

    Creates "reg_coefficients.csv" and "macro_coefficients.csv" for a specific Brazilian state.
'''

import sys
import pandas as pd
import numpy as np


if(len(sys.argv) < 3): sys.exit("[error] run: python3 create_coefficients.py <UF> <regional|macroregional>")
if(sys.argv[2] != "regional" and sys.argv[2] != "macroregional"): sys.exit("[error] Invalid value. Valid options: regional macroregional")

# get state abbreviation (UF)
UF = sys.argv[1]

# import the municipality-region-macroregion relations
reg_macro_df = pd.read_csv("../../data/%s/reg_macro_table.csv"%UF).set_index('year')

# import the flow data for each available year
df = pd.read_csv("../../data/%s/template_dashboard/csv/mun_sih_flow.csv"%UF)

reg = sys.argv[2][:-2] # 'region' or 'macroregion'

def get_reg(year, code):
    available_year = year
    if year <= 2011: 
        available_year = 2011
        if(code//10000 == 35): available_year = 2012  # 'SP' case
    if year > 2017: available_year = 2017
    if(reg == 'macroregion'): available_year = 2019
    
    return reg_macro_df[reg_macro_df['code']==code].loc[available_year, reg]

df[reg+'_from'] = df.apply(lambda row: get_reg(row['year'], eval(row['from'])['code']), axis=1)
df[reg+'_to'] = df.apply(lambda row: get_reg(row['year'], eval(row['to'])['code']), axis=1)

print(df[['year', reg+'_from', reg+'_to']])

# group hospitalization data by year and patient origin region
from_df = df[['year', reg+'_from', 'hospitalizations']].groupby(
    by = ['year', reg+'_from'], as_index = False).sum()
from_df = from_df.rename(columns={'hospitalizations': 'from_hospitalizations'})
#from_df = from_df.drop(from_df[get_number_types(from_df[reg+'_from'])].index)

# group hospitalization data by year and target region
to_df = df[['year', reg+'_to', 'hospitalizations']].groupby(
    by = ['year', reg+'_to'], as_index = False).sum()
to_df = to_df.rename(columns={'hospitalizations': 'to_hospitalizations'})

# get patient flow data where origin and destiny municipalities are the same
same = df[reg+'_from']==df[reg+'_to']
self_df = df[['year', reg+'_from', 'hospitalizations']][same]
self_df = self_df.rename(columns={'hospitalizations': 'self_hospitalizations'})
self_df = self_df.groupby(by = ['year', reg+'_from'], as_index = False).sum()

# merge the hospitalizations data into one data frame
final_df = self_df.merge(to_df, how = 'left', left_on = ['year', reg+'_from'], right_on = ['year', reg+'_to'])
final_df = final_df.merge(from_df, how = 'left', left_on = ['year', reg+'_from'], right_on = ['year', reg+'_from'])
final_df = final_df.rename(columns={reg+'_from': 'name'})
final_df = final_df.drop(columns=[reg+'_to'])


final_df['lifo'] = final_df['self_hospitalizations']/final_df['to_hospitalizations']
final_df['lofi'] = final_df['self_hospitalizations']/final_df['from_hospitalizations']

print(final_df)
if(reg == 'region'): final_df.to_csv("../../data/%s/template_dashboard/csv/coefficients/reg.csv"%UF, index=False)
else: final_df.to_csv("../../data/%s/template_dashboard/csv/coefficients/macro.csv"%UF, index=False)
