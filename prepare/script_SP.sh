#!/bin/bash

for diag in skin_diseases mental_disorders eye_ear_diseases musculoskeletal_system parasitic_diseases diabetes external_causes genitourinary_system nervous_system neoplasm circulatory_system pregnancy
do
   python3 create_communities_shapes.py SP macroregional $diag
done
